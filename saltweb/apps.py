import logging

from django.conf import settings
from django.apps import AppConfig

import redis


_logger = logging.getLogger()


def sget(key, default=False):
    res = getattr(settings, key, None) or default
    return res


class SaltwebConfig(AppConfig):
    name = 'saltweb'

    def ready(self):
        redis_host = sget("REDIS_HOST", "localhost")
        redis_port = sget("REDIS_PORT", 6379)
        redis_db = sget("REDIS_DB", 0)
        try:
            self.redis = r = redis.Redis(
                host=redis_host, port=redis_port, db=redis_db
            )
            if not r.hgetall("global"):
                r.hmset("global", {
                    "visits": 0,
                })
        except redis.exceptions.ConnectionError as err:
            self.redis = None
            _logger.error(
                f"Could not connect to redis. Error was: {str(err)}"
            )
