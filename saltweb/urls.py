from django.urls import path
from django.views.generic import RedirectView

from . import views

app_name = 'saltweb'
urlpatterns = [
    path('', RedirectView.as_view(url="index")),
    path('index', views.IndexView.as_view(), name='index'),
    path('thanks', views.Thanks.as_view()),
    path('experiences', views.ExperiencesList.as_view()),
]
