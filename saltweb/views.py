import os
from datetime import datetime

from django.views.generic import TemplateView
from django.views.generic.edit import CreateView
from django.views.generic.list import ListView
from django.apps import apps
from django.shortcuts import redirect

from .forms import UserExperienceForm
from .models import UserExperienceModel


class IndexView(CreateView):
    template_name = 'saltweb/index.html'
    success_url = '/saltweb/thanks'
    form_class = UserExperienceForm

    def get_context_data(self, **kwargs):
        app = apps.get_app_config('saltweb')
        hostname = os.uname().nodename
        visits_count = 0
        hostname_visits_count = 0
        if app.redis:
            visits_count = app.redis.hget("global", "visits").decode()
            hostname_visits_count = app.redis.hget(hostname, "visits").decode()

            if not app.redis.hgetall(hostname):
                app.redis.hmset(hostname, {
                    "visits": 0,
                })

            app.redis.hincrby("global", "visits", 1)
            app.redis.hincrby(hostname, "visits", 1)

        context = super().get_context_data(**kwargs)
        context.update({
            'hostname': hostname,
            'visits_count': visits_count,
            'hostname_visits_count': hostname_visits_count,
        })
        return context

    def post(self, request, **kwargs):
        form = self.get_form()
        exp = form.save(commit=False)
        exp.date = datetime.now()
        exp.save()
        return redirect(self.success_url)


class Thanks(TemplateView):
    template_name = 'saltweb/thanks.html'


class ExperiencesList(ListView):
    template_name = 'saltweb/experiences.html'
    model = UserExperienceModel
