from django.db import models


class UserExperienceModel(models.Model):
    name = models.CharField(max_length=255)
    experience = models.TextField()
    date = models.DateTimeField()

    def __str__(self):
        return self.name
