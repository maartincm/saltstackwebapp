from django import forms

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit

from .models import UserExperienceModel


class UserExperienceForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(UserExperienceForm, self).__init__(*args, **kwargs)
        self.fields['name'].label = 'Nombre:'
        self.fields['experience'].label = 'Qué tul?'
        self.helper = FormHelper()
        self.helper.form_id = 'id-exampleForm'
        self.helper.form_class = 'blueForms'
        self.helper.form_method = 'post'
        self.helper.form_action = 'index'

        self.helper.add_input(
            Submit('submit', 'Submit', css_class="float-right")
        )

    class Meta:
        model = UserExperienceModel
        fields = [
            'name',
            'experience',
        ]
