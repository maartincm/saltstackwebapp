import yaml


def get_db_info(fname):
    with open(fname, 'r') as fp:
        data_dict = yaml.load(fp.read())
    print(data_dict)
    return data_dict
